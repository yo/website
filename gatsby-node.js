const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const TimelineData = require("./content/timeline.json")
const StackData = require("./content/stack.json")

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  // Define a template for blog post
  const blogPost = path.resolve(`./src/templates/blog-post.tsx`)

  // Get all markdown blog posts sorted by date
  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: ASC }
          limit: 1000
        ) {
          nodes {
            id
            fields {
              slug
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading your blog posts`,
      result.errors
    )
    return
  }

  const posts = result.data.allMarkdownRemark.nodes

  if (posts.length > 0) {
    posts.forEach((post, index) => {
      const previousPostId = index === 0 ? null : posts[index - 1].id
      const nextPostId = index === posts.length - 1 ? null : posts[index + 1].id

      createPage({
        path: post.fields.slug,
        component: blogPost,
        context: {
          id: post.id,
          previousPostId,
          nextPostId,
        },
      })
    })
  }
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })

    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

exports.sourceNodes = ({ actions, createNodeId, createContentDigest }) => {
  const stackList = StackData.stack

  stackList.forEach(stack => {
    const node = {
      name: stack.name,
      description: stack.description,
      url: stack.url,
      logo: stack.logo,
      id: createNodeId(`Stack-${stack.name}`),
      internal: {
        type: "Stack",
        contentDigest: createContentDigest(stack),
      },
    }
    actions.createNode(node)
  })

  const timelineList = TimelineData.timeline

  timelineList.forEach(timeline => {
    const node = {
      title: timeline.title,
      type: timeline.type,
      date: timeline.date,
      link: timeline.link,
      amount: timeline.amount,
      id: createNodeId(`Timeline-${timeline.title}`),
      internal: {
        type: "Timeline",
        contentDigest: createContentDigest(timeline),
      },
    }
    actions.createNode(node)
  })
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  createTypes(`
    type SiteSiteMetadata {
      siteUrl: String
    }

    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
      fields: Fields
    }

    type Frontmatter {
      title: String
      description: String
      date: Date @dateformat
    }

    type Fields {
      slug: String
    }
  `)
}
