import React from "react"
import { Link } from "gatsby"

const Header: React.FC = () => {
  interface NavItemProps {
    url: string
    name: string
  }

  const NavItem = ({ url, name }: NavItemProps) => {
    return (
      <Link
        to={url}
        activeClassName="font-black text-black"
        className="px-3 py-1 rounded-md cursor-pointer hover:text-black hover:bg-gray-200"
      >
        {name}
      </Link>
    )
  }

  const NavItems = () => {
    return (
      <>
        <NavItem url="/" name="Home" />
        <NavItem url="/blog" name="Blog" />
        <NavItem url="/stack" name="Stack" />
      </>
    )
  }

  return (
    <nav>
      <div className="container mx-auto max-w-screen-2xl">
        <div className="relative flex items-center justify-between h-14 sm:h-16">
          <div className="flex-1 flex items-center justify-start">
            <div className="flex-shrink-0 flex items-center space-x-3">
              <Link to="/">
                <img
                  className="rounded-lg"
                  src="/avatar.png"
                  width={40}
                  height={40}
                  alt="Logo"
                />
              </Link>
            </div>
            <div className="hidden sm:block sm:ml-6">
              <div className="flex items-center space-x-4">
                <NavItems />
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Header
