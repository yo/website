import * as React from "react"

interface Props {
  stack: {
    id: string
    name: string
    description: string
    url: string
    logo: string
  }
}

const Stack: React.FC<Props> = ({ stack }) => {
  return (
    <a
      key={stack.id}
      className="border rounded-lg p-5 bg-white"
      href={stack.url}
      target="_blank"
    >
      <img src={stack.logo} className="h-10 w-10 rounded-lg" />
      <div className="font-bold text-blue-600 hover:text-blue-700 mt-3">
        {stack.name}
      </div>
      <div className="text-sm text-gray-500 mt-1">{stack.description}</div>
    </a>
  )
}

export default Stack
