import clsx from "clsx"
import React, {
  ButtonHTMLAttributes,
  DetailedHTMLProps,
  forwardRef,
} from "react"

interface Props
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  size?: "sm" | "md" | "lg"
  icon?: React.ReactNode
  children?: React.ReactNode
  className?: string
}

export const Button = forwardRef<HTMLButtonElement, Props>(function Button(
  { className = "", size = "md", icon, children, ...rest },
  ref
) {
  return (
    <button
      ref={ref}
      className={clsx(
        {
          "px-2 py-0.5": size === "sm",
          "px-3 py-1": size === "md",
          "px-4 py-1.5": size === "lg",
          "flex items-center space-x-1.5": icon && children,
        },
        "border border-gray-300 focus:ring-gray-400 rounded-lg font-bold text-sm shadow-sm focus:ring-2 focus:ring-opacity-50 focus:ring-offset-1 outline-none",
        className
      )}
      {...rest}
    >
      {icon}
      <div>{children}</div>
    </button>
  )
})
