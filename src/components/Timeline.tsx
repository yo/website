import React from "react"
import {
  BriefcaseIcon,
  CubeIcon,
  ExternalLinkIcon,
  FireIcon,
  GiftIcon,
} from "@heroicons/react/outline"

interface Props {
  timelineList: any
}

const Timeline: React.FC<Props> = ({ timelineList }) => {
  return (
    <div className="space-y-5">
      <div className="text-2xl font-black">Timeline</div>
      <div className="space-y-5">
        {timelineList.map(timeline => (
          <div key={timeline.id} className="flex items-center space-x-3">
            {timeline.type === "ACHIEVEMENT" && (
              <div className="bg-yellow-300 text-yellow-600 rounded-full p-1.5">
                <FireIcon className=" h-4" />
              </div>
            )}
            {timeline.type === "LAUNCH" && (
              <div className="bg-pink-300 text-pink-600 rounded-full p-1.5">
                <CubeIcon className=" h-4" />
              </div>
            )}
            {timeline.type === "PRIZE" && (
              <div className="bg-purple-300 text-purple-600 rounded-full p-1.5">
                <GiftIcon className=" h-4" />
              </div>
            )}
            {timeline.type === "NEW_JOB" && (
              <div className="bg-blue-300 text-blue-600 rounded-full p-1.5">
                <BriefcaseIcon className=" h-4" />
              </div>
            )}
            <div className="space-y-1">
              <div className="flex items-center space-x-1">
                <div>{timeline.title}</div>
                {timeline.link && (
                  <a href={timeline.link} target="_blank">
                    <ExternalLinkIcon className="h-4 text-blue-600" />
                  </a>
                )}
              </div>
              <div className="text-xs text-green-600 font-bold">
                {timeline.amount}
              </div>
              <div className="text-xs text-gray-500 pt-1">{timeline.date}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Timeline
