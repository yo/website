import React, { useEffect, useState } from "react"

const Stat = ({ title, stat }: { title: string; stat: string | null }) => (
  <div className="border rounded-lg p-5 bg-white space-y-1">
    <div className="text-sm text-gray-500 mt-1">{title}</div>
    {stat ? (
      <div className="text-xl font-bold">{stat}</div>
    ) : (
      <div className="pt-2">
        <div className="animate-pulse h-5 w-28 rounded bg-gray-300" />
      </div>
    )}
  </div>
)

const Spotify = ({ data }) => (
  <div className="border rounded-lg p-5 bg-white space-y-1">
    <div className="text-sm text-gray-500 mt-1">
      {data ? (
        <div className="flex items-center space-x-1.5">
          <img src="/spotify.svg" className="h-4 w-4" />
          {data.playing ? (
            <span className="text-green-600 font-normal">Listening to </span>
          ) : (
            <span className="text-gray-500 font-normal">Last listened to </span>
          )}
        </div>
      ) : (
        <div className="animate-pulse h-3.5 w-28 rounded bg-gray-300" />
      )}
    </div>
    {data ? (
      <a
        className="text-xl flex items-center space-x-2 font-bold truncate"
        href={data.url}
        title={`${data.name} by ${data.artist}`}
        target="_blank"
      >
        <img src={data.image} className="h-6 w-6 rounded" />
        <div>
          {data.name} by {data.artist}
        </div>
      </a>
    ) : (
      <div className="flex items-center space-x-2 pt-2">
        <div className="animate-pulse h-6 w-6 rounded bg-gray-300" />
        <div className="animate-pulse h-5 w-28 rounded bg-gray-300" />
      </div>
    )}
  </div>
)

const Stats: React.FC = () => {
  const API_URL = "https://api.yogi.codes"

  const [wakatimeData, setWakatimeData] = useState(null)
  const [spotifyData, setSpotifyData] = useState(null)
  const [moodData, setMoodData] = useState(null)

  useEffect(() => {
    fetchData("wakatime")
    fetchData("spotify")
    fetchData("mood")
  }, [])

  const fetchData = (type: string) => {
    fetch(`${API_URL}/api/${type}`)
      .then(response => response.json())
      .then(resultData => {
        type === "wakatime" && setWakatimeData(resultData)
        type === "spotify" && setSpotifyData(resultData)
        type === "mood" && setMoodData(resultData)
      })
  }

  return (
    <div className="space-y-5">
      <div className="text-2xl font-black">Stats</div>
      <div className="grid w-full grid-cols-1 gap-4 my-2 sm:grid-cols-2">
        <Stat
          title="Last 7 days coding activity"
          stat={wakatimeData && (wakatimeData.total as string)}
        />
        <Spotify data={spotifyData} />
        <Stat title="Mood" stat={moodData && (moodData.mood as string)} />
        <Stat title="Mood" stat={moodData && (moodData.mood as string)} />
      </div>
    </div>
  )
}

export default Stats
