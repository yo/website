import * as React from "react"
import Header from "../components/Header"

interface Props {
  location: any
  children: React.ReactNode
}

const Layout: React.FC<Props> = ({ location, children }) => {
  // @ts-ignore
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath

  return (
    <div data-is-root-path={isRootPath}>
      <div className="px-4 mx-auto max-w-full lg:max-w-[50%]">
        <Header />
        <main className="pb-5 sm:pb-10">{children}</main>
      </div>
    </div>
  )
}

export default Layout
