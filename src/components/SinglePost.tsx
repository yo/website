import { Link } from "gatsby"
import * as React from "react"

interface Props {
  post: any
}

const SinglePost: React.FC<Props> = ({ post }) => {
  const title = post.frontmatter.title || post.fields.slug

  return (
    <div key={post.fields.slug}>
      <Link to={post.fields.slug}>
        <div className="text-xl font-bold">{title}</div>
        <div className="text-xs text-gray-500">{post.frontmatter.date}</div>
        <p
          className="mt-2"
          dangerouslySetInnerHTML={{
            __html: post.frontmatter.description || post.excerpt,
          }}
        />
      </Link>
    </div>
  )
}

export default SinglePost
