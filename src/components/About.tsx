import React from "react"

const About: React.FC = () => {
  return (
    <div className="py-10 space-y-2">
      <div className="text-4xl font-black">yoginth.eth</div>
      <div>
        Frontend @{" "}
        <a className="font-bold" href="https://cred.club" target="_blank">
          CRED
        </a>{" "}
        and <b>Web3</b> Developer
      </div>
    </div>
  )
}

export default About
