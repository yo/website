import * as React from "react"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import About from "../components/About"
import Timeline from "../components/Timeline"
import { graphql } from "gatsby"
import Stats from "../components/Stats"

interface Props {
  data: any
  location: any
}

const HomePage: React.FC<Props> = ({ data, location }) => {
  const timelineList = data.allTimeline.nodes

  return (
    <Layout location={location}>
      <SEO />
      <About />
      <div className="space-y-10">
        <Stats />
        <Timeline timelineList={timelineList} />
      </div>
    </Layout>
  )
}

export default HomePage

export const pageQuery = graphql`
  query {
    allTimeline {
      nodes {
        id
        title
        type
        link
        amount
        date
      }
    }
  }
`
