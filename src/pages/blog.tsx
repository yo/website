import * as React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import SinglePost from "../components/SinglePost"

interface Props {
  data: any
  location: any
}

const BlogPage: React.FC<Props> = ({ data, location }) => {
  const posts = data.allMarkdownRemark.nodes

  return (
    <Layout location={location}>
      <SEO title="Blog" />
      <div className="py-10 space-y-2">
        <div className="text-4xl font-black">Blog</div>
        <div>I write mostly about web development, blockchains and devops.</div>
      </div>
      <div className="space-y-10">
        {posts.map(post => (
          <SinglePost post={post} />
        ))}
      </div>
    </Layout>
  )
}

export default BlogPage

export const pageQuery = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      nodes {
        excerpt
        fields {
          slug
        }
        frontmatter {
          date(formatString: "MMMM DD, YYYY")
          title
          description
        }
      }
    }
  }
`
