import { graphql } from "gatsby"
import * as React from "react"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import Stack from "../components/Stack"

interface Props {
  data: any
  location: any
}

const StackPage: React.FC<Props> = ({ data, location }) => {
  const stackList = data.allStack.nodes

  return (
    <Layout location={location}>
      <SEO title="Stack" />
      <div className="py-10 space-y-2">
        <div className="text-4xl font-black">Stack</div>
        <div>My personal and professional softwares and tools I use daily.</div>
      </div>
      <div className="grid w-full grid-cols-1 gap-4 my-2 mt-4 sm:grid-cols-2">
        {stackList.map(stack => (
          <Stack stack={stack} />
        ))}
      </div>
    </Layout>
  )
}

export default StackPage

export const pageQuery = graphql`
  query {
    allStack {
      nodes {
        id
        name
        description
        url
        logo
      }
    }
  }
`
